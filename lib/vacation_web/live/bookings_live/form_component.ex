defmodule VacationWeb.BookingLive.FormComponent do
  use VacationWeb, :live_component

  import Ecto.Changeset

  alias Vacation.Bookings
  alias VacationWeb.BookingForm

  @impl true
  def update(assigns, socket) do
    changeset = change_booking(%{})

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"booking_form" => booking_params}, socket) do
    changeset =
      change_booking(booking_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"booking_form" => booking_params}, socket) do
    save_booking(socket, booking_params)
  end

  defp save_booking(socket, booking_params) do
    changeset = change_booking(booking_params)

    case Bookings.add(get_booking(changeset)) do
      :ok ->
        {:noreply,
         socket
         |> assign(:bookings, Bookings.list())
         |> put_flash(:info, "Booking created successfully")
         |> push_patch(to: "/")}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp change_booking(params) do
    %BookingForm{}
    |> Ecto.Changeset.cast(params, [:from, :until])
    |> validate_required([:from, :until])
    |> validate_dates()
  end

  defp validate_dates(changeset) do
    case compare_dates(get_booking(changeset)) do
      :gt -> add_error(changeset, :until, "must be after start date")
      _ -> changeset
    end
  end

  defp get_booking(changeset) do
    from = get_field(changeset, :from)
    until = get_field(changeset, :until)

    {from, until}
  end

  defp compare_dates({nil, _}), do: :gt
  defp compare_dates({_, nil}), do: :gt
  defp compare_dates({from, until}), do: Date.compare(from, until)
end
