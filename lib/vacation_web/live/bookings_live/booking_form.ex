defmodule VacationWeb.BookingForm do
  use Ecto.Schema

  embedded_schema do
    field(:from, :date, default: Date.utc_today())
    field(:until, :date, default: Date.utc_today())
  end
end
