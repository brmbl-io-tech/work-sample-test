defmodule VacationWeb.BookingLive.Index do
  use VacationWeb, :live_view

  alias Vacation.Bookings
  alias Vacation.Calendar

  @impl true
  def mount(_params, _session, socket) do
    bookings = list_bookings()

    {
      :ok,
      socket
      |> assign(:bookings, bookings)
      |> assign(:available_ranges, find_available_ranges(bookings))
    }
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Bookings")
  end

  defp list_bookings do
    Bookings.list()
  end

  defp find_available_ranges(bookings) do
    Calendar.available(bookings)
  end
end
