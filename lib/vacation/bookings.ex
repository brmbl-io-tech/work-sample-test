defmodule Vacation.Bookings do
  use GenServer

  alias Phoenix.PubSub

  @name :bookings_server

  @start_value []

  # Client

  def topic do
    "vacation_bookings"
  end

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, @start_value, name: @name)
  end

  def add(booking) do
    GenServer.cast(@name, {:add_booking, booking})
  end

  def list() do
    GenServer.call(@name, :list_bookings)
  end

  # Server (callbacks)

  def init(list) do
    {:ok, list}
  end

  def handle_call(:list_bookings, _, bookings) do
    {:reply, bookings, bookings}
  end

  def handle_cast({:add_booking, booking}, state) do
    PubSub.broadcast(Vacation.PubSub, topic(), {:add_booking, booking})
    {:noreply, [booking | state]}
  end
end
