defmodule Vacation.CalendarTest do
  use Vacation.DataCase

  alias Vacation.Calendar

  describe "Calendar" do
    test "available/2 finds one free range inside given events" do
      events = [
        {~D[2022-01-01], ~D[2022-01-01]},
        {~D[2022-01-03], ~D[2022-01-03]}
      ]

      assert Calendar.available(events) == [{~D[2022-01-02], ~D[2022-01-02]}]
    end

    test "available/2 finds multiple free ranges inside given events" do
      events = [
        {~D[2022-01-06], ~D[2022-01-08]},
        {~D[2022-01-01], ~D[2022-01-01]},
        {~D[2022-01-03], ~D[2022-01-03]}
      ]

      assert Calendar.available(events) == [
               {~D[2022-01-02], ~D[2022-01-02]},
               {~D[2022-01-04], ~D[2022-01-05]}
             ]
    end

    test "available/2 finds no free ranges when overlaps" do
      events = [
        {~D[2022-01-06], ~D[2022-01-08]},
        {~D[2022-01-01], ~D[2022-01-01]},
        {~D[2022-01-01], ~D[2022-01-08]},
        {~D[2022-01-03], ~D[2022-01-03]}
      ]

      assert Calendar.available(events) == []
    end

    test "available/2 finds no free ranges when multiple overlaps" do
      events = [
        {~D[2022-01-06], ~D[2022-01-08]},
        {~D[2022-01-01], ~D[2022-01-01]},
        {~D[2022-01-01], ~D[2022-01-08]},
        {~D[2022-01-03], ~D[2022-01-03]},
        {~D[2022-01-02], ~D[2022-01-08]}
      ]

      assert Calendar.available(events) == []
    end

    test "available/2 finds no free ranges when multiple overlaps on end date" do
      events = [
        {~D[2022-01-03], ~D[2022-01-03]},
        {~D[2022-01-06], ~D[2022-01-08]},
        {~D[2022-01-01], ~D[2022-01-08]},
        {~D[2022-01-01], ~D[2022-01-10]}
      ]

      assert Calendar.available(events) == []
    end

    test "available/2 finds no free ranges one main overlapping range" do
      events = [
        {~D[2022-08-05], ~D[2022-08-05]},
        {~D[2022-08-07], ~D[2022-09-07]},
        {~D[2022-01-07], ~D[2024-12-07]}
      ]

      assert Calendar.available(events) == []
    end

    test "available/2 returns nothing when all overlap" do
      events = [
        {~D[2022-01-04], ~D[2022-01-05]},
        {~D[2022-01-05], ~D[2022-01-06]}
      ]

      assert Calendar.available(events) == []
    end

    test "available/2 handles duplicate events" do
      events = [
        {~D[2022-01-01], ~D[2022-01-01]},
        {~D[2022-01-01], ~D[2022-01-01]},
        {~D[2022-01-01], ~D[2022-01-01]}
      ]

      assert Calendar.available(events) == []
    end

    test "available/2 handles empty list" do
      assert Calendar.available([]) == []
    end
  end
end
