defmodule VacationWeb.PageControllerTest do
  use VacationWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Vacations"
  end
end
