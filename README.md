# Bramble Work Sample test

Hi! This is our work sample test for Elixir engineers, designed to help us assess your Elixir/Phoenix/LiveView skills. It's an approximation of the kind of work we do at brmbl.io

## Purpose

This is a Phoenix+LiveView app that lets all users add one or more vacation bookings. It also shows you which date ranges are still available, based on what bookings have been made already.

For this test, we'd like you to clone this project and fix a couple of small issues. You can see our solution in action here: https://vacation-master-4000.fly.dev/

### Problem 1 - Fix the listing page

When you add a new vacation booking, it doesn't reflect in the list without a full page reload.

Fix this so that when a new booking is added, it automatically appears in the Booked list. It should appear for everyone currently connected to the app. You should use PubSub to solve this.

### Problem 2 - 'Available' list

When you add a new booking, the Available list doesn't change.

Instead, the Available list should show a list of date ranges that are still available (i.e. not booked). Use the ranges booked to find your upper and lower bounds.

As an example, if there are these two bookings:

1. 1st Feb -> 5th Feb
1. 9th Feb -> 10th Feb

Then the Available list should show one item: "6th Feb -> 8th Feb". Check out `test/vacation/calendar_test.exs` for some examples of the desired behaviour.

### Bonus Problem - Add a LiveView tests

(This problem is entirely optional)

Add UI (LiveView) test that confirms a booking appears in the list when you press "Save". (Don't worry about testing Available Days.)


## Submit your work

1. Clone this repository locally (please avoid forking it and making your code public)
1. Create a branch for your changes
1. Do some development
1. When you're ready, create a patchset to submit
  - `git diff master <your-branch> > bramble-work-sample.patch.txt`
1. Email the patch to us - using your existing email chain with us

## Deadline

We'd like you to take no more than 1 week to complete this task, and submit your patch.

To be clear: we don’t expect you to code for a whole week! The actual coding time should be closer to *1 or 2* hours, and you have the flexibility to choose when you do the work.

*Please* don’t spend more than 3 hours on your exercise. If you reach 3 hours, and haven’t completed work, please contact us and we’ll help you decide what to do next.

Remember, we use your solution as starting point for a conversation between us. It is not a pass/fail.

## Code and project ownership

This is a test challenge and we have no intent of using the code you have submitted in production. This is your work, and you are free to do whatever you’d like with it.

## Communication

When in doubt, always err on the side of over-communicating. We promise that we are not going to subtract any points for seemingly “silly” questions.

Finally THANK YOU for taking your time to take the challenge. We understand that your time is valuable and we really appreciate it!

